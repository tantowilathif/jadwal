app.controller('tjadwalCtrl', function ($scope, Data, toaster, $modal) {
    //init data
    $scope.form = {};
    var Control_link = 't_jadwal';
    $scope.tampilkan = false;

    Data.get(Control_link + '/getKelas').then(function (data) {
        $scope.getKelas = data.data;
    });
    $scope.view = function (form) {
        Data.get(Control_link + '/view/' + form.id).then(function (data) {
            $scope.dataView = data.data;
            $scope.tampilkan = true;
        });
    }

    $scope.edit = function (form) {
        var modalInstance = $modal.open({
            templateUrl: 'tpl/t_jadwal/modal.html',
            controller: 'modalCtrl',
            size: 'sm',
            backdrop: 'static',
            resolve: {
                form: function () {
                    return form;
                }
            }
        });
        modalInstance.result.then(function (result) {
//            console.log(result)
            result.id = result.kelas_id;
            $scope.view(result);
        }, function () {

        });
    }


    $scope.update = function (form) {
        $scope.is_create = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.id;
        $scope.form = {};
        $scope.form.id = form.id;
        $scope.form.username = form.username;
        $scope.form.nama = form.nama;
        $scope.form.email = form.email;
        $scope.form.password = '';
    };

    $scope.save = function (form) {
        var url = (form.id > 0) ? 'pengguna/update/' : 'pengguna/create';
        Data.post(url, form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $scope.is_edit = false;
                $scope.callServer(tableStateRef); //reload grid ulang
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            }
        });
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete('pengguna/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };


});

app.controller('modalCtrl', function ($state, $scope, toaster, Data, $modalInstance, form) {
//    console.log(form);
//    $scope.form = {};
    $scope.formmodal = form;
    $scope.tanggal = new Date();
    $scope.id = form.id;
    $scope.kelas_id = form.kelas_id;

    $scope.close = function (form) {
//        $modalInstance.dismiss('cancel');
        $modalInstance.close(form);
    };

    $scope.save = function (form) {
        form.id = $scope.id;
        form.kelas_id = $scope.kelas_id;
        if (form.matakuliah == undefined){
            form.matakuliah = null;
        }
        console.log(form);
//        $scope.close(form);
        Data.post('t_jadwal/update', form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $scope.close($scope.formmodal);
            }
        });
    };
});