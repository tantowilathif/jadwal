app.controller('settingCtrl', function($scope, Data, toaster, $state) {
    
    Data.get('app/getsetting').then(function(result) {
        $scope.form = result.data;
    });
    $scope.save = function(form) {
        $scope.authError = null;
        Data.post('app/setting', form).then(function(result) {
            if (result.status == '1') {
                toaster.pop('success', "Berhasil", "Data Berhasil di Simpan");
            } else {
                toaster.pop('error', "Terjadi Kesalahan", "");
            }
        });
    };
})