<?php

post('/app/upload.html', function() {
    if (file_exists("../img/soal/" . $_FILES["upload"]["name"])) {
        echo $_FILES["upload"]["name"] . " already exists please choose another image.";
    } else {
        $newName = date("dYh") . urlParsing($_FILES["upload"]["name"]);
        $uploadPath = img_path() . 'soal' . DIRECTORY_SEPARATOR . $newName;

        move_uploaded_file($_FILES["upload"]["tmp_name"], $uploadPath);

        $url = img_url() . 'soal' . DIRECTORY_SEPARATOR . $newName;

        // Required: anonymous function reference number as explained above.
        $funcNum = $_GET['CKEditorFuncNum'];
        // Optional: instance name (might be used to load a specific configuration file or anything else).
        $CKEditor = $_GET['CKEditor'];
        // Optional: might be used to provide localized messages.
        $langCode = $_GET['langCode'];

        echo "<script type='text/javascript'> window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '');</script>";
    }
});

get('/app/coba', function () {

    for ($i = 0; $i < 10; $i++) {

        echo "<br> Line to show.";
        echo str_pad('', 4096) . "\n";

        ob_flush();
        flush();
        sleep(2);
    }

    echo "Done.";

    ob_end_flush();
//    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});
get('/app/getsetting', function () {
    check_access(array('admin' => true));

    $sql = new LandaDb();
    $models = $sql->select("*")
            ->from('m_setting')
            ->where("=", "id", 1)
            ->find();
    $models->foto = 'img/logo/' . $models->foto;

    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

post('/app/setting', function () {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;
    /** Buat folder untuk masing2 kategori */
    $folder = '../img/logo/';
    if (!file_exists($folder)) {
        mkdir($folder, 0777);
    }

    $sql = new LandaDb();
    $cek = $sql->select("*")->from("m_setting")->where("=", "id", 1)->find();

    if ($data['foto'] != 'img/logo/' . $cek->foto) {

//        $upload = base64toImg($data['foto'], 'app/img/logo/', date("ymdhis") . 'foto');
        $upload = base64toImg($data['foto'], '../img/logo/');
        if ($upload['status']) {
            delImg("../img/logo/", $data['id']);
            $data['foto'] = $upload['data'];
        }
        $params['foto'] = $data['foto'];
    } else {
        unset($params['foto']);
    }
    $model = $sql->update("m_setting", $params, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});
