angular.module('app').run(
        ['$rootScope', '$state', '$stateParams', 'Data',
            function ($rootScope, $state, $stateParams, Data) {
                $rootScope.$state = $state;
                $rootScope.$stateParams = $stateParams;
                //pengecekan login
                $rootScope.$on("$stateChangeStart", function (event, toState) {
                    Data.get('site/session').then(function (results) {
                        if (typeof results.data.user != "undefined") {
                            $rootScope.user = results.data.user;
                        } else {
                            $state.go("access.signin");
                        }
                    });
                });
            }
        ]).config(
        ['$stateProvider', '$urlRouterProvider',
            function ($stateProvider, $urlRouterProvider) {
                $urlRouterProvider.otherwise('/site/dashboard');
                $stateProvider.state('site', {
                    abstract: true,
                    url: '/site',
                    templateUrl: 'tpl/app.html'
                }).state('site.dashboard', {
                    url: '/dashboard',
                    templateUrl: 'tpl/dashboard.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/site/dashboard.js');
                            }
                        ]
                    }
                })
                        // others
                        .state('access', {
                            url: '/access',
                            template: '<div ui-view class="fade-in-right-big smooth"></div>'
                        }).state('access.signin', {
                    url: '/signin',
                    templateUrl: 'tpl/page_signin.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/site/site.js').then();
                            }
                        ]
                    }
                }).state('access.404', {
                    url: '/404',
                    templateUrl: 'tpl/page_404.html'
                }).state('access.forbidden', {
                    url: '/forbidden',
                    templateUrl: 'tpl/page_forbidden.html'
                })
                        //master
                        .state('master', {
                            url: '/master',
                            templateUrl: 'tpl/app.html'

                        }).state('master.penggunaprofile', {
                    url: '/penggunaprofile',
                    templateUrl: 'tpl/m_user/profile.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/m_user/pengguna_profile.js');
                            }
                        ]
                    }
                }).state('master.setting', {
                    url: '/setting',
                    templateUrl: 'tpl/setting.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/site/setting.js');
                            }
                        ]
                    }
                }).state('master.user', {
                    url: '/user',
                    templateUrl: 'tpl/m_user/index.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/m_user/pengguna.js');
                            }
                        ]
                    }
                }).state('master.jam', {
                    url: '/jam',
                    templateUrl: 'tpl/m_jam/index.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/m_jam/jam.js');
                            }
                        ]
                    }
                }).state('master.kelas', {
                    url: '/kelas',
                    templateUrl: 'tpl/m_kelas/index.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/m_kelas/kelas.js');
                            }
                        ]
                    }
                }).state('master.ruangan', {
                    url: '/ruangan',
                    templateUrl: 'tpl/m_ruangan/ruangan.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/m_ruangan/ruangan.js');
                            }
                        ]
                    }
                }).state('transaksi', {
                    url: '/transaksi',
                    templateUrl: 'tpl/app.html'

                }).state('transaksi.jadwal', {
                    url: '/jadwal',
                    templateUrl: 'tpl/t_jadwal/index.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/t_jadwal/jadwal.js');
                            }
                        ]
                    }
                }).state('transaksi.ruangan', {
                    url: '/ruangan',
                    templateUrl: 'tpl/t_ruangan/index.html',
                    resolve: {
                        deps: ['$ocLazyLoad',
                            function ($ocLazyLoad) {
                                return $ocLazyLoad.load('tpl/t_ruangan/ruangan.js');
                            }
                        ]
                    }
                })
            }
        ]);
