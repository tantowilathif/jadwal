<?php

function cek_validate($data, $validasi) {
    if (!empty($custom)) {
        $validasi = $custom;
    }

    $validate = Validator::is_valid($data, $validasi);

    if ($validate === true) {
        return true;
    } else {
        $error = '';
        foreach ($validate as $val) {
            $error .= $val . '<br>';
        }
        return $error;
    }
}

function check_access($param = array('admin' => true, 'login' => false, 'module' => '', '*'), $redirect = '') {
    $r = false;
    if (isset($param['admin']) and $param['admin'] == true) {
        if (isset($_SESSION['user']['id']))
            $r = true;
        else
            $r = false;
    }

    if (isset($param['login']) and $param['login'] === true) {
        if (isset($_SESSION['user']))
            $r = true;
        else
            $r = false;
    }

    if (isset($param['module']) and ! empty($param['module'])) {
        if (isset($_SESSION['user']['akses']) && (in_array([$param['module']], $_SESSION['user']['akses']) || $_SESSION['user']['roles_id'] == "-1"))
            $r = true;
        else
            $r = false;
    }

    if (isset($param['*'])) {
        $r = true;
    }

    if ($r == false) {
        if (empty($redirect))
            not_found();
        else
            redirect($redirect);
    }
}

function summary_tes($tes_det) {


    $total = $total_jwb = $total_belum_jwb = $jwb_benar = $jwb_salah = 0;
    $nilai_materi = $jwb_materi = $nilai_essay = [];
    foreach ($tes_det as $val) {
        $nilai_essay[$val->ujian_det_id] += $val->essay_nilai; 

        if (empty($val->jawaban_pilih)) {
            $total_belum_jwb++;
        } else {
            $total_jwb++;
        }

        if (empty($nilai_materi[$val->ujian_det_id])) {
            $nilai_materi[$val->ujian_det_id] = 0;
        }
        if (empty($jwb_materi[$val->ujian_det_id])) {
            $jwb_materi[$val->ujian_det_id] = 0;
        }

        if ($val->jawaban == $val->jawaban_pilih) {
            $jwb_benar++;
            $nilai_materi[$val->ujian_det_id] += $val->nilai_benar;
        } else {
            $jwb_salah++;
        }

        if ($val->jawaban_pilih == 'a') {
            $jwb_materi[$val->ujian_det_id] += $val->nilai_a;
        } else if ($val->jawaban_pilih == 'b') {
            $jwb_materi[$val->ujian_det_id]+= $val->nilai_b;
        } else if ($val->jawaban_pilih == 'c') {
            $jwb_materi[$val->ujian_det_id]+= $val->nilai_c;
        } else if ($val->jawaban_pilih == 'd') {
            $jwb_materi[$val->ujian_det_id]+= $val->nilai_d;
        } else if ($val->jawaban_pilih == 'e') {
            $jwb_materi[$val->ujian_det_id]+= $val->nilai_e;
        }


        $total++;
    }

//    return ['total'=>$total, 'total_jwb'=>$total_jwb, 'total_belum_jwb'=>$total_belum_jwb, 'jwb_benar'=>$jwb_benar, 'jwb_salah'=>$jwb_salah, 'nilai_kelompok'=>$nilai_kelompok,'nilai_materi'=>$nilai_materi];
    return ['total' => $total, 'total_jwb' => $total_jwb, 'total_belum_jwb' => $total_belum_jwb, 'jwb_benar' => $jwb_benar, 'jwb_salah' => $jwb_salah, 'nilai_materi' => $nilai_materi, 'jwb_materi' => $jwb_materi, 'nilai_essay'=>$nilai_essay];
}

//function hasil_tes(){
//    $tes_det = $sql->select("tes_det.*")
//            ->from("tes_det")
//            ->join("INNER JOIN", "m_materi", "m_materi.id = tes_det.materi_id")
//            ->join("INNER JOIN", "m_kelompok", "m_kelompok.id = m_materi.id_kelompok")
//            ->where("=", "tes_id", $_SESSION['peserta']['tes_id'])
//            ->orderBy('nomor ASC')
//            ->findAll();
//    
//    foreach ($tes_det as $val){
//        
//        if ($val->jawaban == $val->jawaban_pilih){
//            $jwb_benar++;
//        }else{
//            $jwb_salah++;
//        }
//        $total++;
//    }
//}

function bulan($bulan) {
    Switch ($bulan) {
        case 1 : $bulan = "Januari";
            Break;
        case 2 : $bulan = "Februari";
            Break;
        case 3 : $bulan = "Maret";
            Break;
        case 4 : $bulan = "April";
            Break;
        case 5 : $bulan = "Mei";
            Break;
        case 6 : $bulan = "Juni";
            Break;
        case 7 : $bulan = "Juli";
            Break;
        case 8 : $bulan = "Agustus";
            Break;
        case 9 : $bulan = "September";
            Break;
        case 10 : $bulan = "Oktober";
            Break;
        case 11 : $bulan = "November";
            Break;
        case 12 : $bulan = "Desember";
            Break;
    }
    return $bulan;
}

function ConverToRoman($num) {
    $n = intval($num);
    $res = '';

    $romanNumber_Array = [
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1,
    ];

    foreach ($romanNumber_Array as $roman => $number) {
        $matches = intval($n / $number);

        $res .= str_repeat($roman, $matches);

        $n = $n % $number;
    }

    return $res;
}

function CheckUserApprove($nilai, $id_unker) {
    $sql = new LandaDb();
    $id = $sql->find("select * from m_unker where id='{$id_unker}'");
    $sql->select("m_roles.nama as roles_nama,m_user.nama as user_nama, m_user.id as user_id")
            ->from("m_roles")
            ->join("LEFT JOIN", "m_akses_unker", "m_roles.id = m_akses_unker.m_roles_id")
            ->join("LEFT JOIN", "m_user", "m_user.m_roles_id = m_roles.id")
            ->join("LEFT JOIN", "m_unker", "m_unker.id = m_akses_unker.m_unker_id")
            ->customWhere("(m_roles.min_acc < {$nilai} and m_roles.is_deleted =0) and ( m_unker.id='{$id->parent_id}'  OR m_roles.id =6)")
            ->orderBy("m_roles.min_acc ASC");
    $sql->log();
//            $sql->run("SELECT m_roles.nama as roles_nama,m_user.nama as user_nama, m_user.id as user_id FROM m_roles LEFT JOIN m_akses_unker ON m_roles.id = m_akses_unker.m_roles_id LEFT JOIN m_user ON m_user.m_roles_id = m_roles.id LEFT JOIN m_unker ON m_unker.id = m_akses_unker.m_unker_id WHERE (m_roles.min_acc < 6500000 and (m_unker.id='{$id_unker}' OR m_roles.id =6) AND m_roles.is_deleted=0) ORDER BY m_roles.id ASC");
    $model = $sql->findAll();
    $arr = [
        "approve1",
        "approve2",
        "approve3"];
    foreach ($model as $key => $val) {
        $model[$key] = (array) $val;
//        $model[$key]['status'] = $arr[$key];
    }
    return $model;
}

function terbilang($x) {
    $x = abs($x);
    $angka = array("", "satu", "dua", "tiga", "empat", "lima",
        "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = terbilang($x - 10) . " belas";
    } else if ($x < 100) {
        $temp = terbilang($x / 10) . " puluh" . terbilang($x % 10);
    } else if ($x < 200) {
        $temp = " seratus" . terbilang($x - 100);
    } else if ($x < 1000) {
        $temp = terbilang($x / 100) . " ratus" . terbilang($x % 100);
    } else if ($x < 2000) {
        $temp = " seribu" . terbilang($x - 1000);
    } else if ($x < 1000000) {
        $temp = terbilang($x / 1000) . " ribu" . terbilang($x % 1000);
    } else if ($x < 1000000000) {
        $temp = terbilang($x / 1000000) . " juta" . terbilang($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = terbilang($x / 1000000000) . " milyar" . terbilang(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = terbilang($x / 1000000000000) . " trilyun" . terbilang(fmod($x, 1000000000000));
    }
    return $temp;
}

function AngkatoHuruf($angka) {
    $tes = terbilang($angka);
    return ucfirst(strtolower(substr($tes, 1)));
}

function array_sort($array, $on, $order = SORT_ASC) {
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
                break;
            case SORT_DESC:
                arsort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}
