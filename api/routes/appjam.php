<?php

get('/appjam/index', function() {
    check_access(array('admin' => true));
    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id ASC";
    $offset = 0;
    $limit = 10;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort.=" ASC";
            else
                $sort.=" DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("*")
            ->from('m_waktu')
            ->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

    //filter
    $where = '';
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }
//    $sql->log();
    $models = $sql->findAll();
    $totalItem = $sql->count();
    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => (array) $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});

post('/appjam/create', function() {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);

    $sql = new LandaDb();
    $model = $sql->run("INSERT INTO m_waktu (waktu_mulai,waktu_selesai) "
            . "VALUES('$params[waktu_mulai]','$params[waktu_selesai]')");

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

post('/appjam/update', function($id) {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $sql = new LandaDb();
    $model = $sql->update('m_waktu', $data, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

get('/appjam/view/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb2();
    $model = $sql->find("select * from emp where nik=$id");
    unset($model->password);
    echo json_encode(array('status' => 1, 'data' => $model), JSON_PRETTY_PRINT);
});

del('/appjam/delete/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb();
    $model = $sql->delete('m_waktu', array('id' => $id));
    echo json_encode(array('status' => 1));
});
