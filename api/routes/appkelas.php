<?php

get('/appkelas/index', function() {

    check_access(array('admin' => true));

    //init variable
    $params = $_REQUEST;
    $filter = array();
    $sort = "id DESC";
    $offset = 0;
    $limit = 10;

    //limit & offset pagination
    if (isset($params['limit']))
        $limit = $params['limit'];
    if (isset($params['offset']))
        $offset = $params['offset'];

    //sorting
    if (isset($params['sort'])) {
        $sort = $params['sort'];
        if (isset($params['order'])) {
            if ($params['order'] == "false")
                $sort.=" ASC";
            else
                $sort.=" DESC";
        }
    }

    $sql = new LandaDb();
    $sql->select("*")
            ->from('m_kelas')
            ->limit($limit)
            ->orderBy($sort)
            ->offset($offset);

    //filter
    $where = '';
    if (isset($params['filter'])) {
        $filter = (array) json_decode($params['filter']);
        foreach ($filter as $key => $val) {
            $sql->where('LIKE', $key, $val);
        }
    }
//    $sql->log();
    $models = $sql->findAll();
    $totalItem = $sql->count();
    $sql->clearQuery();

    echo json_encode(array('status' => 1, 'data' => (array) $models, 'totalItems' => $totalItem), JSON_PRETTY_PRINT);
});

post('/appkelas/create', function() {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $sql = new LandaDb();
    $model = $sql->insert('m_kelas', $data);

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

post('/appkelas/update', function($id) {

    check_access(array('admin' => true));

    $params = json_decode(file_get_contents("php://input"), true);
    $data = $params;

    $sql = new LandaDb();
    $model = $sql->update('m_kelas', $data, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});

del('/appkelas/delete/:id', function($id) {

    check_access(array('admin' => true));

    $sql = new LandaDb();
    $model = $sql->delete('m_kelas', array('id' => $id));
    echo json_encode(array('status' => 1));
});
