<?php

get('/t_jadwal/getKelas', function() {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $models = $sql->findAll("SELECT * FROM m_kelas");
    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

get('/t_jadwal/view/:kelas_id', function($kelas_id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $cek = $sql->select("*")->from("t_jadwal")->where("=", "kelas_id", $kelas_id)->find();
    if (!$cek) {
        $waktu = $sql->select("*")->from("m_waktu")->findAll();
        foreach ($waktu as $val) {
            for ($i = 1; $i < 6; $i++) {
                if ($i == 1){
                    $hari = 'Senin';
                }elseif ($i == 2) {
                    $hari = 'Selasa';
                }elseif ($i == 3) {
                    $hari = 'Rabu';
                }elseif ($i == 4) {
                    $hari = 'Kamis';
                }elseif ($i == 5) {
                    $hari = 'Jumat';
                }
                $insert = $sql->insert("t_jadwal", array("waktu_id" => $val->id, "kelas_id" => $kelas_id, "hari_id" => $i, "hari" => $hari));
            }
            $i = 1;
        }
    } else {
        
    }

    $models = $sql->select("*")->from("m_waktu")->findAll();
    
    foreach ($models as $key => $val) {
        $models[$key] = (array) $val;
        
        $jadwal = $sql->select("*")
                ->from("t_jadwal")
                ->where("=", "kelas_id", $kelas_id)
                ->andWhere("=", "waktu_id", $val->id)
                ->orderBy("hari_id ASC")
                ->findAll();
        
        $models[$key]['detail'] = $jadwal;
    }
    
    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

post('/t_jadwal/update', function() {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);

    $sql = new LandaDb();
    $model = $sql->update('t_jadwal', $params, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});