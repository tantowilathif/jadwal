app.controller('truanganCtrl', function ($scope, Data, toaster, $modal) {
    //init data
    $scope.form = {};
    $scope.form.hari = 'Senin';
    var Control_link = 't_ruangan';
    $scope.tampilkan = false;

    $scope.view = function (form) {
        console.log(form);
        Data.get(Control_link + '/view/' + form).then(function (data) {
            $scope.dataView = data.data;
//            $scope.tampilkan =true;
        });
    }
    $scope.view('Senin');

    $scope.edit = function (form) {
        console.log(form)
        var modalInstance = $modal.open({
            templateUrl: 'tpl/t_ruangan/modal.html',
            controller: 'modalCtrl',
            size: 'md',
            backdrop: 'static',
            resolve: {
                form: function () {
                    return form;
                }
            }
        });
        modalInstance.result.then(function (result) {
//            console.log(result)
            var data = result.hari;
            $scope.view(data);
        }, function () {

        });
    }


    $scope.update = function (form) {
        $scope.is_create = false;
        $scope.is_edit = true;
        $scope.is_view = false;
        $scope.formtitle = "Edit Data : " + form.id;
        $scope.form = {};
        $scope.form.id = form.id;
        $scope.form.username = form.username;
        $scope.form.nama = form.nama;
        $scope.form.email = form.email;
        $scope.form.password = '';
    };

    $scope.save = function (form) {
        var url = (form.id > 0) ? 'pengguna/update/' : 'pengguna/create';
        Data.post(url, form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $scope.is_edit = false;
                $scope.callServer(tableStateRef); //reload grid ulang
                toaster.pop('success', "Berhasil", "Data berhasil tersimpan");
            }
        });
    };

    $scope.delete = function (row) {
        if (confirm("Apa anda yakin akan MENGHAPUS PERMANENT item ini ?")) {
            Data.delete('pengguna/delete/' + row.id).then(function (result) {
                $scope.displayed.splice($scope.displayed.indexOf(row), 1);
            });
        }
    };


});

app.controller('modalCtrl', function ($state, $scope, toaster, Data, $modalInstance, form) {
    console.log(form);
//    $scope.form = {};
    $scope.formmodal = form;
    $scope.tanggal = new Date();
    $scope.id = form.id;
    $scope.kelas_id = form.kelas_id

    if (form.t_jadwal_id == false) {
        form.t_jadwal_id = "";
        form.kelas_id = "";
    }

    $scope.open1 = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened1 = true;
    };

    Data.get('t_jadwal/getKelas').then(function (data) {
        $scope.getKelas = data.data;
    });

    $scope.getJadwal = function (form) {
        Data.get('t_ruangan/getJadwal/' + form.id).then(function (data) {
            if (data.status == 1) {
                $scope.jadwal = data.data;
            } else {
                $scope.jadwal = undefined;
                toaster.pop('error', "Terjadi Kesalahan", "Belum Ada Jadwal");
            }
        });
    }

    $scope.hapus = function (id) {
        Data.get('t_ruangan/hapus/' + id).then(function (data) {
            if (data.status == 1) {
                $scope.close($scope.formmodal);
            } else {
                toaster.pop('error', "Terjadi Kesalahan", "Belum Ada Jadwal");
            }
        });
    };
    
    $scope.close = function (form) {
        $modalInstance.close($scope.formmodal);
    };

    $scope.save = function (form) {
        form.t_jadwal_id = form.t_jadwal_id.id;
        Data.post('t_ruangan/update', form).then(function (result) {
            if (result.status == 0) {
                toaster.pop('error', "Terjadi Kesalahan", result.errors);
            } else {
                $scope.close($scope.formmodal);
            }
        });
    };
});