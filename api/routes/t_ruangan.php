<?php

get('/t_ruangan/getJadwal/:kelas_id', function($kelas_id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $models = $sql->findAll("SELECT * FROM t_jadwal WHERE kelas_id = $kelas_id AND matakuliah IS NOT NULL");

    if ($models) {
        echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'data' => (array) $models), JSON_PRETTY_PRINT);
    }
});

get('/t_ruangan/getKelas', function() {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $models = $sql->findAll("SELECT * FROM m_kelas");
    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

get('/t_ruangan/hapus/:id', function($id) {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $models = $sql->update("t_ruang", array("t_jadwal_id" => 'null', "status" => 'Tetap', "tanggal" => '', "lama_pemakaian" => '', "alasan" => ''), array("id" => $id));
    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

get('/t_ruangan/view/:hari', function($hari) {
    check_access(array('admin' => true));
    $sql = new LandaDb();

    $cek = $sql->select("*")->from("t_ruang")->where("=", "hari", "$hari")->find();
    if (!$cek) {
        $ruang = $sql->select("*")->from("m_ruang")->findAll();
        foreach ($ruang as $val) {
            $waktu = $sql->select("*")->from("m_waktu")->findAll();
            foreach ($waktu as $vals) {
                $insert = $sql->insert("t_ruang", array("waktu_id" => $vals->id, "ruang_id" => $val->id, "hari" => $hari));
            }
            $i = 1;
        }
    } else {
        
    }

    $models = $sql->select("t_ruang.*,m_waktu.waktu_mulai,m_waktu.waktu_selesai,m_ruang.nama AS nama_ruang")
            ->from("t_ruang")
            ->join("left join", "m_waktu", "m_waktu.id = t_ruang.waktu_id")
            ->join("left join", "m_ruang", "m_ruang.id = t_ruang.ruang_id")
            ->where("=", "t_ruang.hari", $hari)
            ->groupBy("t_ruang.ruang_id")
            ->orderBy("nama_ruang ASC")
            ->findAll();
    $sql2 = new LandaDb();
    $sql3 = new LandaDb();
    foreach ($models as $key => $val) {
        $models[$key] = (array) $val;
        $detail = $sql2->select("t_ruang.*,m_waktu.waktu_mulai,m_waktu.waktu_selesai,m_ruang.nama AS nama_ruang,t_jadwal.kelas_id,t_jadwal.matakuliah,m_kelas.id as kelas_id, m_kelas.nama as nama_kelas,m_kelas.prog_keahlian,m_kelas.semester")
                ->from("t_ruang")
                ->join("left join", "m_waktu", "m_waktu.id = t_ruang.waktu_id")
                ->join("left join", "m_ruang", "m_ruang.id = t_ruang.ruang_id")
                ->join("left join", "t_jadwal", "t_jadwal.id = t_ruang.t_jadwal_id")
                ->join("left join", "m_kelas", "m_kelas.id = t_jadwal.kelas_id")
                ->where("=", "t_ruang.hari", $val->hari)
                ->andWhere("=", "t_ruang.ruang_id", $val->ruang_id)
                ->andWhere("=", "t_ruang.hari", $hari)
                ->orderBy("m_waktu.id ASC")
                ->findAll();
        
        foreach ($detail as $keys => $vals) {
            $detail[$keys] = (array) $vals;
            $kelas = $sql3->select("*")->from("m_kelas")->where("=", "id", $vals->kelas_id)->find();
            $jadwal = $sql3->select("*")->from("t_jadwal")->where("=", "id", $vals->t_jadwal_id)->find();
            $detail[$keys]['kelas_id'] = $kelas;
            $detail[$keys]['t_jadwal_id'] = $jadwal;
        }

        $models[$key]['detail'] = $detail;
    }

    echo json_encode(array('status' => 1, 'data' => (array) $models), JSON_PRETTY_PRINT);
});

post('/t_ruangan/update', function() {
    check_access(array('admin' => true));
    $params = json_decode(file_get_contents("php://input"), true);

    $sql = new LandaDb();
    $model = $sql->update('t_ruang', $params, array('id' => $params['id']));

    if ($model) {
        echo json_encode(array('status' => 1, 'data' => (array) $model), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => 'Data gagal disimpan'), JSON_PRETTY_PRINT);
    }
});
