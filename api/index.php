<?php

session_start();
// error_reporting(1);

require 'vendor/autoload.php';

/* --- System --- */
require 'lib/database.php';
require 'lib/dispatch.php';
require 'lib/functions.php';

/* --- Library --- */
require 'lib/validator.php';

config('source', 'config.ini');

$uri = dispatch();

$fl = explode("/", $uri);
$file = 'routes/' . $fl[0] . '.php';

if (file_exists($file)) {
    require $file;
}else {
    require 'routes/sites.php';
}
get('.*', function() {
    not_found();
});

route(method(), "/{$uri}");


