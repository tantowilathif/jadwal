<?php

get('/site/getLogo', function () {

    $sql = new LandaDb();
    $models = $sql->select("*")->from("m_setting")->where("=", "id", 1)->find();

    echo json_encode(array('status' => 1, 'data' => $models), JSON_PRETTY_PRINT);
});

get('/index', function () {
    header("location:../");
    exit;
});

get('/site/session', function () {
    if (isset($_SESSION['user']['id'])) {
        echo json_encode(array('status' => 1, 'data' => array_filter($_SESSION)), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 1, 'data' => 'undefined'), JSON_PRETTY_PRINT);
    }
});

get('/site/sessionPeserta', function () {
    if (isset($_SESSION['peserta']['id'])) {
        echo json_encode(array('status' => 1, 'data' => array_filter($_SESSION)), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 1, 'data' => 'undefined'), JSON_PRETTY_PRINT);
    }
});

post('/site/login', function () {
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();
    $model = $sql->select("*")
            ->from("m_user")
            ->where("=", "username", $params['username'])
            ->andWhere("=", "password", sha1($params['password']))
            ->find();
    if (!empty($model)) {
        $_SESSION['user']['id'] = $model->id;
        $_SESSION['user']['no_ujian'] = $model->no_ujian;
        $_SESSION['user']['username'] = $model->username;
        $_SESSION['user']['nama'] = $model->nama;
        $_SESSION['user']['roles_id'] = $model->roles_id;

        echo json_encode(array('status' => 1, 'data' => array_filter($_SESSION)), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => "Authentication Systems gagal, username atau password Anda salah."), JSON_PRETTY_PRINT);
    }
});

post('/site/loginPeserta', function () {


    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();

    //buat ngetes
    //$sql->delete('tes');
    //$sql->delete('tes_det');

    //pengecekan nip ada tidak
    $id_ujian = $sql->select("*")
            ->from("ujian")
            ->where("=", "pin_sesi", $params['pin_sesi'])
            ->find();
    $model = $sql->select("*")
            ->from("m_peserta")
            ->where("=", "no_ujian", $params['no_ujian'])
            ->andWhere("=", "ujian_id", $id_ujian->id)
            ->find();

    if (!empty($model)) {
        //pengecekan ujiannya ada tidak
        $model_ujian = $sql->select("*")
                ->from("ujian")
                ->where("=", "pin_sesi", $params['pin_sesi'])
                ->find();
        if (!empty($model_ujian)) {
            if ($model_ujian->status == 0) {
                echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => "Authentication Systems gagal, PIN sesi anda telah berakhir! Segera hubungi administrator untuk masalah ini"), JSON_PRETTY_PRINT);
            } else {
                $_SESSION['peserta']['id'] = $model->id;
                $_SESSION['peserta']['nama'] = $model->nama;
                $_SESSION['peserta']['tanggal_lahir'] = $model->tanggal_lahir;
                $_SESSION['peserta']['no_ujian'] = $model->no_ujian;
                $_SESSION['peserta']['pin_sesi'] = $model_ujian->pin_sesi;
                $_SESSION['peserta']['ujian_id'] = $model_ujian->id;

                $model_tes = $sql->select("*")
                        ->from("tes")
                        ->where("=", "peserta_id", $_SESSION['peserta']['id'])
                        ->andWhere("=", "ujian_id", $_SESSION['peserta']['ujian_id'])
                        ->find();

                if (empty($model_tes)) {
                    $form = [];
                    $form['ujian_id'] = $_SESSION['peserta']['ujian_id'];
                    $form['peserta_id'] = $_SESSION['peserta']['id'];
                    $form['time_mulai'] = time();
                    $form['time_selesai'] = time() + ($model_ujian->durasi * 60);
                    $form['status'] = 1;
                    $tes = $sql->insert("tes", $form);

                    $_SESSION['peserta']['tes_id'] = $tes->id;

                    $ujiandet = $sql->select("*")
                            ->from("ujian_det")
                            ->where("=", "id_ujian", $_SESSION['peserta']['ujian_id'])
                            ->findAll();

                    $nomor = 1;
                    foreach ($ujiandet as $val) {
                        $model_soal = $sql->select("m_soal.*, m_materi.id_kelompok")
                                ->from("m_soal")
                                ->join("INNER JOIN", "m_materi", "m_materi.id = m_soal.id_materi")
                                ->where("=", "level", $val->level)
                                ->andWhere("=", "id_materi", $val->id_materi)
                                ->limit($val->jumlah_soal)
                                ->orderBy('RAND()')
                                ->findAll();
                                //->log();

                        $soal_cerita = [];
                        foreach ($model_soal as $val_soal) {
                            if ($val_soal->id_soal_cerita > 0){ //jika soal cerita
                                $soal_cerita[$val_soal->id_soal_cerita][$val_soal->id] = $val_soal;
                            }else{
                                $form = [];
                                $form['tes_id'] = $tes->id;
                                $form['nomor'] = $nomor;
                                $form['pertanyaan'] = $val_soal->pertanyaan;
                                $form['id_soal'] = $val_soal->id;
                                $form['id_soal_cerita'] = $val_soal->id_soal_cerita;

                                /* ============pengacakan isi soal============ */
                                $input = [];
                                if (!empty($val_soal->jawaban_a))
                                    $input[] = $val_soal->jawaban_a;
                                if (!empty($val_soal->jawaban_b))
                                    $input[] = $val_soal->jawaban_b;
                                if (!empty($val_soal->jawaban_c))
                                    $input[] = $val_soal->jawaban_c;
                                if (!empty($val_soal->jawaban_d))
                                    $input[] = $val_soal->jawaban_d;
                                if (!empty($val_soal->jawaban_e))
                                    $input[] = $val_soal->jawaban_e;

                                shuffle($input);
                                $isi_jawaban = '';
                                if ($val_soal->jawaban == 'a') {
                                    $isi_jawaban = $val_soal->jawaban_a;
                                } else if ($val_soal->jawaban == 'b') {
                                    $isi_jawaban = $val_soal->jawaban_b;
                                } else if ($val_soal->jawaban == 'c') {
                                    $isi_jawaban = $val_soal->jawaban_c;
                                } else if ($val_soal->jawaban == 'd') {
                                    $isi_jawaban = $val_soal->jawaban_d;
                                } else if ($val_soal->jawaban == 'e') {
                                    $isi_jawaban = $val_soal->jawaban_e;
                                }

                                //memindahkan jawaban_pilihan (single answer) dan nilai per jwb (skala)
                                $abjad = 'a';
                                foreach ($input as $key => $val_input) {
                                    $form['jawaban_' . $abjad] = $val_input;

                                    if ($val->cara_penilaian == 1) {
                                        if ($isi_jawaban == $val_input)
                                            $form['jawaban'] = $abjad;
                                        /*$form['jawaban'] = 'a'; //cara cek masuk gak, kuncinya*/
                                    } else {
                                        if ($val_soal->jawaban_a == $val_input && !empty($val_soal->jawaban_a)) {
                                            $form['nilai_' . $abjad] = $val->nilai_a;
                                        } else if ($val_soal->jawaban_b == $val_input && !empty($val_soal->jawaban_b)) {
                                            $form['nilai_' . $abjad] = $val->nilai_b;
                                        } else if ($val_soal->jawaban_c == $val_input && !empty($val_soal->jawaban_c)) {
                                            $form['nilai_' . $abjad] = $val->nilai_c;
                                        } else if ($val_soal->jawaban_d == $val_input && !empty($val_soal->jawaban_d)) {
                                            $form['nilai_' . $abjad] = $val->nilai_d;
                                        } else if ($val_soal->jawaban_e == $val_input && !empty($val_soal->jawaban_e)) {
                                            $form['nilai_' . $abjad] = $val->nilai_e;
                                        }
                                    }

                                    $abjad++;
                                }

                                if ($val->cara_penilaian == 2) {
                                    $form['jawaban'] = $val_soal->jawaban;
                                }

                                /* ===akhir pengacakan soal */
                                $form['kelompok_id'] = $val_soal->id_kelompok;
                                $form['materi_id'] = $val_soal->id_materi;
                                $form['id_soal_cerita'] = $val_soal->id_soal_cerita;

                                $form['ujian_det_id'] = $val->id;
                                $form['cara_penilaian'] = $val->cara_penilaian;
                                $form['nilai_benar'] = $val->nilai_benar;

                                $sql->insert("tes_det", $form);
                                $nomor++;
                            }
                        }

                        //mengurutkan val soal cerita, biar yg duluan yang keluar
                        $model_soal_cerita = [];
                        //echo json_encode($soal_cerita);

                        foreach ($soal_cerita as $value1) {
                            foreach ($value1 as $value2) {
                                $model_soal_cerita[] = $value2;
                            }
                        }   
                        //print_r($model_soal_cerita);

                        //copian seperti yang diatas, untuk menaruh soal cerita di nomor paling belakang
                        foreach ($model_soal_cerita as $val_soal) {
                                $form = [];
                                $form['tes_id'] = $tes->id;
                                $form['nomor'] = $nomor;
                                $form['pertanyaan'] = $val_soal->pertanyaan;
                                $form['id_soal'] = $val_soal->id;
                                $form['id_soal_cerita'] = $val_soal->id_soal_cerita;

                                /* ============pengacakan isi soal============ */
                                $input = [];
                                if (!empty($val_soal->jawaban_a))
                                    $input[] = $val_soal->jawaban_a;
                                if (!empty($val_soal->jawaban_b))
                                    $input[] = $val_soal->jawaban_b;
                                if (!empty($val_soal->jawaban_c))
                                    $input[] = $val_soal->jawaban_c;
                                if (!empty($val_soal->jawaban_d))
                                    $input[] = $val_soal->jawaban_d;
                                if (!empty($val_soal->jawaban_e))
                                    $input[] = $val_soal->jawaban_e;

                                shuffle($input);
                                $isi_jawaban = '';
                                if ($val_soal->jawaban == 'a') {
                                    $isi_jawaban = $val_soal->jawaban_a;
                                } else if ($val_soal->jawaban == 'b') {
                                    $isi_jawaban = $val_soal->jawaban_b;
                                } else if ($val_soal->jawaban == 'c') {
                                    $isi_jawaban = $val_soal->jawaban_c;
                                } else if ($val_soal->jawaban == 'd') {
                                    $isi_jawaban = $val_soal->jawaban_d;
                                } else if ($val_soal->jawaban == 'e') {
                                    $isi_jawaban = $val_soal->jawaban_e;
                                }


                                //memindahkan jawaban_pilihan (single answer) dan nilai per jwb (skala)
                                $abjad = 'a';
                                foreach ($input as $key => $val_input) {
                                    $form['jawaban_' . $abjad] = $val_input;

                                    if ($val->cara_penilaian == 1) {
                                        if ($isi_jawaban == $val_input)
                                            $form['jawaban'] = $abjad;
                                        /*$form['jawaban'] = 'a'; //cara cek masuk gak, kuncinya*/
                                    } else {
                                        if ($val_soal->jawaban_a == $val_input && !empty($val_soal->jawaban_a)) {
                                            $form['nilai_' . $abjad] = $val->nilai_a;
                                        } else if ($val_soal->jawaban_b == $val_input && !empty($val_soal->jawaban_b)) {
                                            $form['nilai_' . $abjad] = $val->nilai_b;
                                        } else if ($val_soal->jawaban_c == $val_input && !empty($val_soal->jawaban_c)) {
                                            $form['nilai_' . $abjad] = $val->nilai_c;
                                        } else if ($val_soal->jawaban_d == $val_input && !empty($val_soal->jawaban_d)) {
                                            $form['nilai_' . $abjad] = $val->nilai_d;
                                        } else if ($val_soal->jawaban_e == $val_input && !empty($val_soal->jawaban_e)) {
                                            $form['nilai_' . $abjad] = $val->nilai_e;
                                        }
                                    }

                                    $abjad++;
                                }

                                if ($val->cara_penilaian == 2) {
                                    $form['jawaban'] = $val_soal->jawaban;
                                }

                                /* ===akhir pengacakan soal */

                                $form['kelompok_id'] = $val_soal->id_kelompok;
                                $form['materi_id'] = $val_soal->id_materi;
                                $form['id_soal_cerita'] = $val_soal->id_soal_cerita;

                                $form['ujian_det_id'] = $val->id;
                                $form['cara_penilaian'] = $val->cara_penilaian;
                                $form['nilai_benar'] = $val->nilai_benar;

                                $sql->insert("tes_det", $form);
                                $nomor++;
                        }
                        
                        //print_r($soal_cerita);
                        




                    }
                } else {
                    $_SESSION['peserta']['tes_id'] = $model_tes->id;
                }
                echo json_encode(array('status' => 1, 'data' => array_filter($_SESSION)), JSON_PRETTY_PRINT);
            }
        } else {
            echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => "Authentication Systems gagal, PIN sesi salah! Segera hubungi administrator untuk mengetahui PIN sesi yang benar"), JSON_PRETTY_PRINT);
        }
    } else {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => "Authentication Systems gagal, Nomor Ujian peserta tidak ditemukan"), JSON_PRETTY_PRINT);
    }
});

get('/site/logout', function () {
    session_destroy();
});

//pengecekan biodata ujian
post('/site/cekBiodata', function () {
    $params = json_decode(file_get_contents("php://input"), true);
    $sql = new LandaDb();


    $cek = $sql->select("m_peserta.*, ujian.kode_ujian,nama_ujian,lokasi_ujian")
            ->from("m_peserta")
            ->join("LEFT JOIN", "ujian", "ujian.id = m_peserta.ujian_id")
            ->where("=", "m_peserta.no_ujian", $params['no_ujian'])
            ->andWhere("=", "ujian.pin_sesi", $params['pin_sesi'])
            ->andWhere("=", "ujian.status", 1)
            ->find();

    if (empty($cek)) {
        echo json_encode(array('status' => 0, 'error_code' => 400, 'errors' => "Authentication Systems gagal, Nomor Ujian / PIN Sesi tidak ditemukan"), JSON_PRETTY_PRINT);
    } else {
        echo json_encode(array('status' => 1, 'data' => $cek), JSON_PRETTY_PRINT);
    }
});
